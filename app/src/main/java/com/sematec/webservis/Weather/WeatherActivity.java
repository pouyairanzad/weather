package com.sematec.webservis.Weather;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.sematec.webservis.PublicMethods;
import com.sematec.webservis.R;
import com.sematec.webservis.Weather.Models.Forecast;
import com.sematec.webservis.Weather.Models.YahooModel;

import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

public class WeatherActivity extends AppCompatActivity implements View.OnClickListener {
    EditText city;
    Context mContext =this;
    TextView temp;
    String Ip_url = "http://ip-api.com/json";
    ListView forcastsListview;
    ProgressDialog dialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        bind();
        getCityName();
        findViewById(R.id.show).setOnClickListener(this);

    }
    void bind(){
        temp=findViewById(R.id.temp);
        city=findViewById(R.id.city);
        forcastsListview=findViewById(R.id.forecasts);
        dialog =new   ProgressDialog(mContext);
        dialog.setTitle("Loading");
        dialog.setMessage("Please Wait");

    }






    @Override
    public void onClick(View view) {
        getCityTempFromYahoo(city.getText().toString());


    }
    void getCityName(){
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Ip_url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.Toast(mContext,throwable.toString() );

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                ParsAndSetCity(responseString);

            }
        });
    }
    void ParsAndSetCity (String ServerResponse){
        try {

            JSONObject jsonObject =new JSONObject(ServerResponse);
            String CityVal = jsonObject.getString("city").toLowerCase();
            getCityTempFromYahoo(CityVal);
            city.setText(CityVal);

        }
        catch ( Exception ex){
            PublicMethods.Toast(mContext,"data kill");

        }


    }
    void getCityTempFromYahoo(String CityName){
        String Url= "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" + CityName +"   %2Cir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        dialog.show();
        AsyncHttpClient clinet = new AsyncHttpClient();
        clinet.get(Url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.Toast(mContext,"data not connect");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parsAndSetTempYahoo(responseString);

            }

            @Override
            public void onFinish() {
                super.onFinish();
                dialog.dismiss();
            }
        });
    }
    void parsAndSetTempYahoo(String YahooResponse){
        Gson gson =new Gson();
        YahooModel yahooModel=gson.fromJson(YahooResponse,YahooModel.class);
        String TempValue = yahooModel.getQuery().getResults().getChannel().getItem().getCondition().getTemp();
        temp.setText(PublicMethods.ftoc(TempValue)+"F");
        List<Forecast> forecasts = yahooModel.getQuery().getResults().getChannel().getItem().getForecast();
        ForecastAdapter adapter = new ForecastAdapter(mContext,forecasts);
        forcastsListview.setAdapter(adapter);


    }

}


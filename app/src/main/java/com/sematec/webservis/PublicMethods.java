package com.sematec.webservis;

import android.content.Context;
import android.widget.Toast;

public class PublicMethods {
    public  static void Toast (Context mContext , String msg ){
        Toast.makeText(mContext,msg, Toast.LENGTH_SHORT).show();
    }
    public static  float ftoc(String f){
        float intf = Integer.parseInt(f);
        return (float) ((float) Math.round(intf -32 ) /1.8);

    }
}

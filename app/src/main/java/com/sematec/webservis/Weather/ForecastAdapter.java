package com.sematec.webservis.Weather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sematec.webservis.R;
import com.sematec.webservis.Weather.Models.Forecast;

import java.util.List;

public class ForecastAdapter extends BaseAdapter {
    Context mContext ;
    List<Forecast> forecasts;

    public ForecastAdapter(Context mContext, List<Forecast> forecasts) {
        this.mContext = mContext;
        this.forecasts = forecasts;
    }

    @Override
    public int getCount() {
        return forecasts.size();
    }


    @Override
    public Object getItem(int i) {
        return forecasts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.forecasts_list_item,viewGroup,false);
        TextView day = v.findViewById(R.id.day);
        TextView high = v.findViewById(R.id.high);
        TextView low = v.findViewById(R.id.low);




        TextView text = v.findViewById(R.id.text);
        ImageView icon = v.findViewById(R.id.icon);
        day.setText(forecasts.get(i).getDay());
        text.setText(forecasts.get(i).getText());
        low.setText(forecasts.get(i).getLowC()+ "C");
        high.setText(forecasts.get(i).getHighC()+ "C");

         if (forecasts.get(i).getCode().equals("32")){
            setImage(icon,"https://cdn0.iconfinder.com/data/icons/weather-forecast-18/50/Sunny_Sun_Weather_Forecast-512.png");
        } else if (forecasts.get(i).getCode().equals("5")) {
             setImage(icon, "https://cdn0.iconfinder.com/data/icons/weather-forecast-18/51/Sleet_Ice_Snow_Cold_Winter_Weather_Forecast-512.png");
         }
        else if (forecasts.get(i).getCode().equals("16")){
            setImage(icon,"https://cdn0.iconfinder.com/data/icons/weather-forecast-18/49/Snow_Snowing_Snowflakes_Cold_Winter_Weather_Forecast-512.png");
        }
        else if (forecasts.get(i).getCode().equals("24")){

            setImage(icon,"https://cdn0.iconfinder.com/data/icons/weather-forecast-18/40/Precipitation_Humidity_Waves_Water_Weather_Forecast-512.png");
        }
        else if (forecasts.get(i).getCode().equals("26")){

            setImage(icon,"https://cdn0.iconfinder.com/data/icons/weather-forecast-18/50/Partly_Cloudy_Day_Weather_Forecast-512.png");
        }




        return v;
    }
    void setImage(ImageView img ,String url){
        Glide.with(mContext).load(url).into(img);
    }
}
